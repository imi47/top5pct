<?php include 'header.php' ?>

<img src="images/firewatch.jpg" alt="" class="full-width-image">

<div class="customer-review">
	<script type="text/javascript">
		var review_token = 'p8zTh9U63lY2CqrsAeYXsRiw4jv5Zjf3dyLYsOhuw31M8yBWDT';
		var review_target = 'review-container'; 
	</script>
	<script src="https://reviewsonmywebsite.com/js/embed.js?v=8" type="text/javascript"></script>
	<div id="review-container"></div>
</div>

<p class="custom-shirt-info">Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsum, perspiciatis fuga expedita,
	earum molestiae porro molestias sequi animi quae neque consequatur corporis commodi. Corporis sit quibusdam animi,
	ratione ullam esse.</p>

<div class="print-options">
	<div>
		<img src="images/1.jpg" alt="" class="item-1">
	</div>
	<div>
		<img src="images/2.jpg" alt="" class="item-2">
	</div>
	<div>
		<img src="images/3.jpg" alt="" class="item-3">
	</div>
	<div>
		<img src="images/firewatch.jpg" alt="" class="item-4">
	</div>
	<p class="item-1">Lorem ipsum, dolor sit amet consectetur adipisicing elit. Sunt eum libero rem illum fuga rerum unde
		magni aliquam tenetur suscipit quam reiciendis quos voluptate nam assumenda, cumque maxime facere ullam.</p>
	<p class="item-2">Lorem ipsum dolor sit amet consectetur adipisicing elit. Delectus sunt aliquid eum suscipit, officia
		nisi est commodi quae, hic nemo quis itaque? Aperiam molestias ipsum alias harum tempora inventore optio.</p>
	<p class="item-3">Lorem ipsum dolor sit amet consectetur adipisicing elit. Aspernatur, placeat.</p>
	<p class="item-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, molestiae rerum consectetur enim
		atque vitae voluptate numquam quas quis placeat a rem saepe itaque ratione voluptatem dignissimos ad quisquam magni
		natus facere! Alias nemo maxime earum tenetur sequi eius ut. Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis, molestiae rerum consectetur enim atque vitae voluptate numquam quas quis placeat a rem saepe itaque ratione voluptatem dignissimos.</p>
</div>

<div class="line"></div>

<div class="pics-with-text">
	<a href="#" data-scroll><img src="images/1.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</a>
	<a href="#" data-scroll><img src="images/2.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</a>
	<a href="#" data-scroll><img src="images/3.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</a>
	<a href="#" data-scroll><img src="images/4.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</a>
	<a href="#" data-scroll><img src="images/firewatch.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</a>
	<a href="#" data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</a>
	<a href="#" data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</a>
	<a href="#" data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</a>
</div>

<iframe class="commercial" src="https://www.youtube.com/embed/j0vslNa91gU" frameborder="0"
	allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<?php include 'footer.php' ?>