<?php include 'header.php' ?>

<?php 
if($_GET['fn']){
	$first_name = $_GET['fn'];
}

?>

<div class="ty-wrapper">
	<div class="ty-content">
		<h1 class>That's all<?php echo $first_name; ?>, thank you!</h1>
		<p class=""> A Top 5% representative will contact you soon. below are the <strong>File Formats accepted</strong></p>
		<ul>
			<li>.jpg <span class="highlight">(resolution 300dpi at printed size)</span></li>
			<li>.svg</li>
			<li>.ai</li>
			<li>.psd</li>
			<li>.eps</li>
			<li>.pdf</li>
		</ul>
		<a href="https://script.google.com/macros/s/AKfycbyV9n_YrelO9qJz1O9fr24AY5WiFsVWDJsVz-P0Gn5efQTWUwyF/exec"
			title="Upload Art to Top 5%" target="_blank" class="upload-art">CLICK HERE TO UPLOAD YOUR ARTWORK!!!</a>
	</div>
</div>

<style>
	.map {
		display: none;
	}
</style>

<?php include 'footer.php' ?>