<?php include 'header.php' ?>

<div class="home-banner">
	<div class="carousel-cell">
		<img src="images/1.jpg" alt="">
	</div>
	<div class="carousel-cell">
		<img src="images/2.jpg" alt="">
	</div>
	<div class="carousel-cell">
		<img src="images/3.jpg" alt="">
	</div>
</div>

<div class="customer-review">
	<script type="text/javascript">
		var review_token = 'p8zTh9U63lY2CqrsAeYXsRiw4jv5Zjf3dyLYsOhuw31M8yBWDT';
		var review_target = 'review-container'; 
	</script>
	<script src="https://reviewsonmywebsite.com/js/embed.js?v=8" type="text/javascript"></script>
	<div id="review-container"></div>
</div>
<div class="who-we-are" data-scroll>
	<h1>Who We Are?</h1>
	<p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Dolorum tempore velit corrupti odit nulla enim
		adipisci earum in perspiciatis illum culpa, accusantium omnis dicta distinctio nesciunt aut ratione illo
		doloremque.</p>
</div>
<div class="pics-with-text">
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="images/1.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="images/2.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="images/3.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="images/4.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="images/firewatch.jpg" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
</div>
<div class="headline">
	<div>
		<span>Get Your Own Custom Store!</span>
		<div>
			<span>Schools</span>
			<span>Businesses</span>
			<span>Family or Class Reunions</span>
			<span>Sport Teams</span>
		</div>
	</div>
	<a href="#">
		<span>DIY and Save More $</span>
		<span>(Design It Yourself )</span>
		<div>
			<span>Use our online designer to create your product</span>
		</div>
		<div>
			<span>Free Clipart and Fonts</span>
			<span>click here to watch video tutorial</span>
		</div>
	</a>
	<a href="#">
		<span>Get Fancy with Specialty Material</span>
		<div>
			<span>Rhinestone</span>
			<span>Reflective</span>
			<span>Glitter</span>
			<span>Holographic</span>
			<span>Glow</span>
			<span>Vinyl</span>
			<span>Foil</span>
			<span>Patterns</span>
			<span>Flock</span>
			<span>Brick</span>
			<span>and more!</span>
		</div>
	</a>
	<div>
		<span>Wider Printing Options</span>
		<div>
			<span>Screen Printing</span>
			<span>Sublimation Printing</span>
			<span>Laser Printing</span>
			<span>Digital Printing</span>
		</div>
	</div>
	<div>
		<span>First Come First Serve</span>
		<span>Same Day Service</span>
		<div>
			<span>Quantity Discounts</span>
		</div>
		<div>
			<span>Plenty of Shirt Styles and Colors</span>
		</div>
		<div>
			<span>Rush Job Ready</span>
		</div>
	</div>
	<div>
		<span>Looking for a specific Brand? Browse our catalogs</span>
		<a href="#">Catalog 1</a>
		<a href="#">Catalog 2</a>
	</div>
</div>

<div class="photo-viewer">
	<div class="carousel-cell">
		<a href="images/1.jpg" data-lightbox='pics'>
			<img src="images/1.jpg" alt="">
		</a>
	</div>
	<div class="carousel-cell">
		<a href="images/firewatch.jpg" data-lightbox='pics'>
			<img src="images/firewatch.jpg" alt="">
		</a>
	</div>
	<div class="carousel-cell">
		<a href="images/firewatch.jpg" data-lightbox='pics'>
			<img src="images/firewatch.jpg" alt="">
		</a>
	</div>
	<div class="carousel-cell">
		<a href="images/firewatch.jpg" data-lightbox='pics'>
			<img src="images/firewatch.jpg" alt="">
		</a>
	</div>
	<div class="carousel-cell">
		<a href="images/firewatch.jpg" data-lightbox='pics'>
			<img src="images/firewatch.jpg" alt="">
		</a>
	</div>
	<div class="carousel-cell">
		<a href="images/firewatch.jpg" data-lightbox='pics'>
			<img src="images/firewatch.jpg" alt="">
		</a>
	</div>
	<div class="carousel-cell">
		<a href="images/firewatch.jpg" data-lightbox='pics'>
			<img src="images/firewatch.jpg" alt="">
		</a>
	</div>
	<div class="carousel-cell">
		<a href="images/firewatch.jpg" data-lightbox='pics'>
			<img src="images/firewatch.jpg" alt="">
		</a>
	</div>
</div>

<div class="two-pics">
	<a href=""><img src="images/1.jpg" alt=""></a>
	<a href=""><img src="https://source.unsplash.com/user/erondu/1600x900" alt=""></a>
</div>
<iframe class="commercial" src="https://www.youtube.com/embed/v4D8-ujSNAE" frameborder="0"
	allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<a href="#" class="answers" data-scroll><span>? ? ? Answers to your questions ? ? ?</span></a>

<script src="js/glide.min.js"></script>
<?php include 'footer.php' ?>