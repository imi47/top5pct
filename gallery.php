<?php include 'header.php' ?>

<div class="gallery-container">
	<h1>Images</h1>
	<div>
		<a href="images/firewatch.jpg">
			<img src="images/firewatch.jpg" />
		</a>
		<a href="images/1.jpg">
			<img src="images/1.jpg" />
		</a>
		<a href="images/2.jpg">
			<img src="images/2.jpg" />
		</a>
		<a href="images/3.jpg">
			<img src="images/3.jpg" />
		</a>
		<a href="images/4.jpg">
			<img src="images/4.jpg" />
		</a>
		<a href="images/firewatch.jpg">
			<img src="images/firewatch.jpg" />
		</a>
		<a href="images/1.jpg">
			<img src="images/1.jpg" />
		</a>
		<a href="images/2.jpg">
			<img src="images/2.jpg" />
		</a>
		<a href="images/3.jpg">
			<img src="images/3.jpg" />
		</a>
		<a href="images/4.jpg">
			<img src="images/4.jpg" />
		</a>
		<a href="images/1.jpg">
			<img src="images/1.jpg" />
		</a>
		<a href="images/2.jpg">
			<img src="images/2.jpg" />
		</a>
		<a href="images/3.jpg">
			<img src="images/3.jpg" />
		</a>
		<a href="images/4.jpg">
			<img src="images/4.jpg" />
		</a>
		<a href="images/firewatch.jpg">
			<img src="images/firewatch.jpg" />
		</a>
		<a href="images/1.jpg">
			<img src="images/1.jpg" />
		</a>
		<a href="images/2.jpg">
			<img src="images/2.jpg" />
		</a>
		<a href="images/3.jpg">
			<img src="images/3.jpg" />
		</a>
		<a href="images/4.jpg">
			<img src="images/4.jpg" />
		</a>
	</div>
</div>

<div class="gallery-container video-gallery-container">
	<h1>Videos</h1>
	<div>
		<a href="https://www.youtube.com/watch?v=SaZiUBfXKEs" data-poster="images/firewatch.jpg">
			<img src="images/firewatch.jpg" />
		</a>
		<a href="https://vimeo.com/1084537" data-poster="images/1.jpg">
			<img src="images/1.jpg" />
		</a>
		<a href="https://www.youtube.com/watch?v=SaZiUBfXKEs" data-poster="images/firewatch.jpg">
			<img src="images/firewatch.jpg" />
		</a>
		<a href="https://vimeo.com/1084537" data-poster="images/1.jpg">
			<img src="images/1.jpg" />
		</a>
		<a href="https://www.youtube.com/watch?v=SaZiUBfXKEs" data-poster="images/firewatch.jpg">
			<img src="images/firewatch.jpg" />
		</a>
		<a href="https://vimeo.com/1084537" data-poster="images/1.jpg">
			<img src="images/1.jpg" />
		</a>
		<a href="https://www.youtube.com/watch?v=SaZiUBfXKEs" data-poster="images/firewatch.jpg">
			<img src="images/firewatch.jpg" />
		</a>
		<a href="https://vimeo.com/1084537" data-poster="images/1.jpg">
			<img src="images/1.jpg" />
		</a>
	</div>
</div>

<?php include 'footer.php' ?>