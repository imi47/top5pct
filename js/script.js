
$('nav > div').on('click', function () {
	if ($('nav > .fa-caret-down').css('display') == 'block') {
		$('nav > div').not(this).children('div').hide('500');
		$(this).children('div').toggle('500');
		$(this).children('div').css('display', 'flex');
		$('nav > div').not(this).removeClass('active');
		$(this).toggleClass('active');
	}
});

$('nav .fa-caret-down').on('click', function () {
	$('nav').toggleClass('hidden');
	$('nav > div').children('div').hide('500');
	$('nav > div').removeClass('active');
});

let is769 = window.matchMedia('(min-width: 769px)');

document.addEventListener('DOMContentLoaded', function () {
	var trigger = new ScrollTrigger({
		toggle: {
			visible: 'in',
			hidden: 'out'
		},
		offset: {
			x: 0,
			y: -50
		},
		addHeight: true,
		once: true
	}, document.body, window);

	var delay = 0;
	$('.pics-with-text div, .pics-with-text a').each(function () {
		$(this).css('transition-delay', delay + 'ms');
		delay += 200;
	});

	var delay = 0;
	$('.swatch-colors div').each(function () {
		$(this).css('transition-delay', `${delay}ms`);
		delay += 100;
	});

	var delay = 0;
	$('.faqs-banner span').each(function () {
		$(this).css('animation-delay', `${delay}ms`);
		delay += 200;
	});

	$('.swatch-colors div').on('mouseenter touchstart', function() {
		$(this).css('transition-delay', '0s');
	});

	$('.flickity-page-dots').parent().css('margin-bottom', '3rem');

	$('.full-width-image').insertBefore('.container');

	$(".gallery-container > div").lightGallery();


	$('.print-options img').on('mouseenter', function () {
		if (is769.matches) {
			var i = $(this).attr('class');
			$(`.print-options > p.${i}`).slideDown('500');
			$(this).addClass('active');
			// $('.print-options > p').css('border-width', '2px');
		}
	});

	$('.print-options img').on('mouseleave', function () {
		if (is769.matches) {
			$(`.print-options > p`).slideUp('500');
			$(this).removeClass('active');
		}
		// $('.print-options > p').css('border-width', '0');
	});

	$('.print-options img').on('click', function () {
		if (!is769.matches) {
			var i = $(this).attr('class');
			$('.print-options p').not(`.${i}`).slideUp('500');
			$(`.print-options > p.${i}`).slideToggle('500');
			$('.print-options img').not(this).parent().removeClass('active');
			$(this).parent().toggleClass('active');
		}
	});
});

$('.input-container input').on('focus', function () {
	$(this).addClass('focused');
});

$('.input-container input').on('blur', function () {
	if ($(this).val().length == 0)
		$(this).removeClass('focused');
});

$('.home-banner').flickity({
	cellAlign: 'left',
	contain: true,
	pauseAutoPlayOnHover: false,
	prevNextButtons: false
});

$('.photo-viewer').flickity({
	cellAlign: 'left',
	contain: true,
	pageDots: false
});

