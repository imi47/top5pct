<div class="mapouter map">
	<div class="gmap_canvas">
		<iframe id="gmap_canvas"
			src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2987.1491083263013!2d-88.11436904853203!3d41.522710679150194!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x880e616bad7e7e49%3A0x2badd3ad261e9b2e!2s1256+W+Jefferson+St%2C+Joliet%2C+IL+60435!5e0!3m2!1sen!2sus!4v1502603987766"
			frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
		</iframe>
	</div>
</div>
</div>
<footer>
	<div class="images">
		<a itemprop="url" href="https://www.cylex.us.com/company/top-5-percent--llc-27024809.html" target="_blank">
			<img itemprop="image" src="https://admin.cylex.us.com/cylex_logo3_27024809.png" alt="CYLEX"
				title="Top 5 Percent, LLC" />
		</a>

		<a itemprop="url" href="https://www.veteranownedbusiness.com" target="_blank"><img itemprop="image"
				src="images/veteran.png" alt="Proud Veteran Owned Business Member!" border="0px" /></a>

		<div id="ssl">
			<!-- (c) 2005, 2017. Authorize.Net is a registered trademark of CyberSource Corporation -->
			<div class="AuthorizeNetSeal">
				<script type="text/javascript"
					language="javascript">var ANS_customer_id = "98003171-16d7-4f57-b541-d9cb36fbc6a8";</script>
				<script type="text/javascript" language="javascript" src="//verify.authorize.net/anetseal/seal.js"></script>
				<a href="http://www.authorize.net/" id="AuthorizeNetText" target="_blank">Online Credit Card Processing</a>
			</div>
		</div>


		<script type="text/javascript"> //<![CDATA[
			var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.trust-provider.com/" : "http://www.trustlogo.com/");
			document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
	//]]></script>
		<script language="JavaScript" type="text/javascript">
			TrustLogo("https://ssl.comodo.com/images/seals/sectigo_trust_seal_lg_2x.png", "SECDV", "none");
		</script>
	</div>

	<div class="footer-content">
		<a href="index.php" class="logo"><img src="images/logo.gif" alt=""></a>
		<div>
			<span>Quick Links</span>
			<a href="#">Terms of Use</a>
			<a href="#">Privacy Policy</a>
			<a href="#">Press Release</a>
			<a href="#">Blog</a>
			<a href="faqs.php">FAQs</a>
		</div>
		<div>
			<span>Top 5 Percent, LLC</span>
			<span>1256 West Jefferson Street, Suite 103</span>
			<span>Joliet, Illinois 60435</span>
			<span>Telephone: (815) 349-8600</span>
		</div>
		<p>All logos, site design, and content ©Copyright 2017 by Top 5%, LLC. (Top 5 Percent, LLC) | All Rights Reserved
			| Top 5%, LLC (Top 5 Percent, LLC) is a Registered Trademark</p>
	</div>

</footer>

<script src="js/jquery-3.4.1.min.js"></script>
<script src="js/lightbox.min.js"></script>
<script src="js/ScrollTrigger.min.js"></script>
<script src="js/flickity.pkgd.min.js"></script>
<script src="js/lightgallery-all.min.js"></script>
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src="js/script.js"></script>
</body>

</html>