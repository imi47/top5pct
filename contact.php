<?php include 'header.php' ?>

<a class="faqs-banner">
	<span>Reach</span>
	<span>Out</span>
	<span>To</span>
	<span>Us</span>
</a>

<div class="customer-review">
	<script type="text/javascript">
		var review_token = 'p8zTh9U63lY2CqrsAeYXsRiw4jv5Zjf3dyLYsOhuw31M8yBWDT';
		var review_target = 'review-container'; 
	</script>
	<script src="https://reviewsonmywebsite.com/js/embed.js?v=8" type="text/javascript"></script>
	<div id="review-container"></div>
</div>

<div class="contact-us">
	<form action="">
		<div class='input-container'>
			<input type="text" name="f-name" id="f-name" required>
			<label for="f-name">First Name</label>
		</div>
		<div class='input-container'>
			<input type="text" name="l-name" id="l-name" required>
			<label for="l-name">Last Name</label>
		</div>
		<div class="input-container">
			<input type="email" name="email" id="email" required>
			<label for="email">Email</label>
		</div>
		<div class="input-container">
			<input type="number" name="phone" id="phone" required>
			<label for="phone">Phone number</label>
		</div>
		<div class="message">
			<label for="message">Message</label>
			<textarea name="message" id="message" required></textarea>
		</div>
		<button type="submit" class="send">Send</button>
	</form>
	<div class="info">
		<div>
			<span><i class="fas fa-map-marker-alt"></i>Address:</span>
			<span>1256 West Jefferson Street Suite 103</span>
			<span>Joliet, Illinois 60435</span>
		</div>
		<div>
			<span><i class="fas fa-phone-alt"></i>Phone:</span>
			<span>(815) 349-8600</span>
		</div>
		<div>
			<span><i class="fas fa-envelope"></i>Email:</span>
			<span>reachoutto@top5pct.com</span>
		</div>
	</div>
</div>
<?php include 'footer.php' ?>