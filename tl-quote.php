<?php include 'header.php' ?>

<?php
	
  $output_form = true; //declare a FLAG we can use to test whether or not to show form
	
  $first_name = NULL;
  $last_name = NULL;
  $phonenumber = NULL;
  $companyname = NULL;
  $email = NULL;
  $streetaddress = NULL;
  $city = NULL;
  $state = NULL;
  $zip = NULL;  
  $description = NULL;
  $other_style = NULL;
  $other_length = NULL;
  $colors = NULL;
  $men_xs = NULL;
  $men_s = NULL;
  $men_m = NULL;
  $men_l = NULL;
  $men_xl = NULL;
  $men_2xl = NULL;
  $men_3xl = NULL;
  $men_4xl = NULL;
  $men_5xl = NULL;
  $men_6xl = NULL;  
  $women_xs = NULL;
  $women_s = NULL;
  $women_m = NULL;
  $women_l = NULL;
  $women_xl = NULL;
  $women_2xl = NULL;
  $women_3xl = NULL;
  $women_4xl = NULL;
  $women_5xl = NULL;
  $women_6xl = NULL;  
  $youth_xs = NULL;
  $youth_s = NULL;
  $youth_m = NULL;
  $youth_l = NULL;
  $youth_xl = NULL;
  $youth_2xl = NULL;
  $youth_3xl = NULL;
  $youth_4xl = NULL;
  $youth_5xl = NULL;
  $youth_6xl = NULL;
  $toddler_xs = NULL;  
  $toddler_s = NULL;
  $toddler_m = NULL;
  $toddler_l = NULL;
  $toddler_xl = NULL;
  $toddler_2xl = NULL;
  $toddler_3xl = NULL;
  $toddler_4xl = NULL;
  $toddler_5xl = NULL;
  $toddler_6xl = NULL;
	
  //sticky check boxes
  $vneck=0;
  $crewneck=0;
  $hoodie=0;
  $shortsleeve=0;
  $longsleeve=0;
  $heavyweight=0;
  $lightweight=0;
  $glitter=0;
  $glitterflake=0;
  $foil=0;
  $vinyl=0;
  $flock=0;
  $rhinestones=0;
  $neon=0;
  $glow=0;
  $reflective=0;
  $hologram=0;
  $pebblepuff=0;
  $glaze=0;
  $patterns=0;
  $twill=0;
  $airflow=0;
  $facebook=0;
  $internet_search=0;
  $referral=0;
	
if (isset($_POST['submit']) ) { //conditional processing based on whether or not the user has submitted.
	
  $dbc = mysqli_connect('localhost', 'ezrawebf_tillman', 'Br@nd@n121968', 'ezrawebf_top5percent_apparel')
    or die('Error connecting to MySQL server.');
	
  $first_name = mysqli_real_escape_string($dbc, trim($_POST['firstname']));
  $last_name = mysqli_real_escape_string($dbc, trim($_POST['lastname']));
  $phonenumber = mysqli_real_escape_string($dbc, trim($_POST['phonenumber']));
  $companyname = mysqli_real_escape_string($dbc, trim($_POST['companyname']));
  $email = mysqli_real_escape_string($dbc, trim($_POST['email']));
  $streetaddress = mysqli_real_escape_string($dbc, trim($_POST['streetaddress']));
  $city = mysqli_real_escape_string($dbc, trim($_POST['city']));
  $state = mysqli_real_escape_string($dbc, trim($_POST['state']));
  $zip = mysqli_real_escape_string($dbc, trim($_POST['zip']));  
  $description = mysqli_real_escape_string($dbc, trim($_POST['description']));
  $other_style = mysqli_real_escape_string($dbc, trim($_POST['other_style']));
  $other_length = mysqli_real_escape_string($dbc, trim($_POST['other_length']));
  $colors = mysqli_real_escape_string($dbc, trim($_POST['colors']));
  $men_xs = mysqli_real_escape_string($dbc, trim($_POST['men_xs']));
  $men_s = mysqli_real_escape_string($dbc, trim($_POST['men_s']));
  $men_m = mysqli_real_escape_string($dbc, trim($_POST['men_m']));
  $men_l = mysqli_real_escape_string($dbc, trim($_POST['men_l']));
  $men_xl = mysqli_real_escape_string($dbc, trim($_POST['men_xl']));
  $men_2xl = mysqli_real_escape_string($dbc, trim($_POST['men_2xl']));
  $men_3xl = mysqli_real_escape_string($dbc, trim($_POST['men_3xl']));
  $men_4xl = mysqli_real_escape_string($dbc, trim($_POST['men_4xl']));
  $men_5xl = mysqli_real_escape_string($dbc, trim($_POST['men_5xl']));
  $men_6xl = mysqli_real_escape_string($dbc, trim($_POST['men_6xl']));  
  $women_xs = mysqli_real_escape_string($dbc, trim($_POST['women_xs']));
  $women_s = mysqli_real_escape_string($dbc, trim($_POST['women_s']));
  $women_m = mysqli_real_escape_string($dbc, trim($_POST['women_m']));
  $women_l = mysqli_real_escape_string($dbc, trim($_POST['women_l']));
  $women_xl = mysqli_real_escape_string($dbc, trim($_POST['women_xl']));
  $women_2xl = mysqli_real_escape_string($dbc, trim($_POST['women_2xl']));
  $women_3xl = mysqli_real_escape_string($dbc, trim($_POST['women_3xl']));
  $women_4xl = mysqli_real_escape_string($dbc, trim($_POST['women_4xl']));
  $women_5xl = mysqli_real_escape_string($dbc, trim($_POST['women_5xl']));
  $women_6xl = mysqli_real_escape_string($dbc, trim($_POST['women_6xl']));  
  $youth_xs = mysqli_real_escape_string($dbc, trim($_POST['youth_xs']));
  $youth_s = mysqli_real_escape_string($dbc, trim($_POST['youth_s']));
  $youth_m = mysqli_real_escape_string($dbc, trim($_POST['youth_m']));
  $youth_l = mysqli_real_escape_string($dbc, trim($_POST['youth_l']));
  $youth_xl = mysqli_real_escape_string($dbc, trim($_POST['youth_xl']));
  $youth_2xl = mysqli_real_escape_string($dbc, trim($_POST['youth_2xl']));
  $youth_3xl = mysqli_real_escape_string($dbc, trim($_POST['youth_3xl']));
  $youth_4xl = mysqli_real_escape_string($dbc, trim($_POST['youth_4xl']));
  $youth_5xl = mysqli_real_escape_string($dbc, trim($_POST['youth_5xl']));
  $youth_6xl = mysqli_real_escape_string($dbc, trim($_POST['youth_6xl']));
  $toddler_xs = mysqli_real_escape_string($dbc, trim($_POST['toddler_xs']));  
  $toddler_s = mysqli_real_escape_string($dbc, trim($_POST['toddler_s']));
  $toddler_m = mysqli_real_escape_string($dbc, trim($_POST['toddler_m']));
  $toddler_l = mysqli_real_escape_string($dbc, trim($_POST['toddler_l']));
  $toddler_xl = mysqli_real_escape_string($dbc, trim($_POST['toddler_xl']));
  $toddler_2xl = mysqli_real_escape_string($dbc, trim($_POST['toddler_2xl']));
  $toddler_3xl = mysqli_real_escape_string($dbc, trim($_POST['toddler_3xl']));
  $toddler_4xl = mysqli_real_escape_string($dbc, trim($_POST['toddler_4xl']));
  $toddler_5xl = mysqli_real_escape_string($dbc, trim($_POST['toddler_5xl']));
  $toddler_6xl = mysqli_real_escape_string($dbc, trim($_POST['toddler_6xl']));

	
     // Verify that the user selected a checkbox
  if ( isset($_POST['vneck']) ) {
	  $vneck=$_POST['vneck'];
  }
  
   if ( isset($_POST['crewneck']) ) {
	  $crewneck=$_POST['crewneck'];
  }
  
   if ( isset($_POST['hoodie']) ) {
	  $hoodie=$_POST['hoodie'];
  }
  if ( isset($_POST['shortsleeve']) ) {
	  $shortsleeve=$_POST['shortsleeve'];
  }
  
   if ( isset($_POST['longsleeve']) ) {
	  $longsleeve=$_POST['longsleeve'];
  }
  
   if ( isset($_POST['heavyweight']) ) {
	  $heavyweight=$_POST['heavyweight'];
  }
  if ( isset($_POST['lightweight']) ) {
	  $lightweight=$_POST['lightweight'];
  }
  
   if ( isset($_POST['glitter']) ) {
	  $glitter=$_POST['glitter'];
  }
  
   if ( isset($_POST['glitterflake']) ) {
	  $glitterflake=$_POST['glitterflake'];
  }
  if ( isset($_POST['foil']) ) {
	  $foil=$_POST['foil'];
  }
   if ( isset($_POST['vinyl']) ) {
	  $vinyl=$_POST['vinyl'];
  }
  
   if ( isset($_POST['flock']) ) {
	  $flock=$_POST['flock'];
  }
  
   if ( isset($_POST['rhinestones']) ) {
	  $rhinestones=$_POST['rhinestones'];
  }
  if ( isset($_POST['neon']) ) {
	  $neon=$_POST['neon'];
  }
  
   if ( isset($_POST['glow']) ) {
	  $glow=$_POST['glow'];
  }
  
   if ( isset($_POST['reflective']) ) {
	  $reflective=$_POST['reflective'];
  }
  if ( isset($_POST['hologram']) ) {
	  $hologram=$_POST['hologram'];
  }
  
   if ( isset($_POST['pebblepuff']) ) {
	  $pebblepuff=$_POST['pebblepuff'];
  }
  
   if ( isset($_POST['glaze']) ) {
	  $glaze=$_POST['glaze'];
  }
  if ( isset($_POST['patterns']) ) {
	  $patterns=$_POST['patterns'];
  }
  
   if ( isset($_POST['twill']) ) {
	  $twill=$_POST['twill'];
  }
  
   if ( isset($_POST['airflow']) ) {
	  $airflow=$_POST['airflow'];
  }
	
  if ( isset($_POST['facebook']) ) {
	  $facebook=$_POST['facebook'];
  }
  
   if ( isset($_POST['internet_search']) ) {
	  $internet_search=$_POST['internet_search'];
  }
  
   if ( isset($_POST['referral']) ) {
	  $referral=$_POST['referral'];
  }
	
	
  //Sticky radio buttons
  if (isset($_POST['yes_or_no'])) {
	  $yesorno=$_POST['yes_or_no'];
  }
  if (isset($_POST['yes_or_no2'])) {
	  $yes_or_no2=$_POST['yes_or_no2'];
  }
	
  $output_form = false; // will only change to TRUE based on validation
	  
//Validate all form fields	
if (empty($first_name)) {
	
	echo "WAIT - The First Name field is blank <br />";
	 $output_form = true; // will print form.
}

if (empty($last_name)) {
	
	echo "WAIT - The Last Name field is blank <br />";
	 $output_form = true; // will print form.
}

if (empty($phonenumber)) {
	
	echo "WAIT - The Phone Number field is blank <br />";
	 $output_form = true; // will print form.
}

if (!is_numeric($phonenumber)) {
	
	echo "WAIT - The Phone Number field has a letter in it or it is blank<br />";
	 $output_form = true; // will print form.
}
	
if (empty($email)) {
	
	echo "WAIT - The Email field is blank <br />";
	 $output_form = true; // will print form.
}
	
if (empty($streetaddress)) {
	
	echo "WAIT - The Street Address field is blank <br />";
	 $output_form = true; // will print form.
}
	
if (empty($city)) {
	
	echo "WAIT - The City field is blank <br />";
	 $output_form = true; // will print form.
}
	
if (empty($state)) {
	
	echo "WAIT - The State field is blank <br />";
	 $output_form = true; // will print form.
}

if (strlen($zip) != 5) {
	
echo "WAIT - The Zipcode is not equal to 5 characters <br />";
     $output_form = true; // will print form.

}
	
if (!is_numeric($zip)) {
	
	echo "WAIT - The Zipcode field has a letter in it or it is blank<br />";
	 $output_form = true; // will print form.
}
	
if (empty($zip)) {
	
	echo "WAIT - The Zipcode field is blank <br />";
	 $output_form = true; // will print form.
}	

if (empty($colors)) {
	
	echo "WAIT - The Color field is blank <br />";
	 $output_form = true; // will print form.
}

if (empty($description)) {
	
	echo "WAIT - The Extra Notes field is blank<br />";
	 $output_form = true; // will print form.
}

  if(isset($_POST['g-recaptcha-response'])){
		
        $captcha=$_POST['g-recaptcha-response'];
        if(!$captcha){
			echo "WAIT - Please fill the captcha<br />";
			$output_form = true; // will print form.
        }else{
			$secret_key = "6LdCXB0UAAAAADI6Kt0LmmP5q2FG8ikAYhgeeOPz";
			$response=json_decode(file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$secret_key."&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']), true);
			if($response['success'] == false){
				echo "WAIT - You are spammer go away.<br />";
				$output_form = true; // will print form.
			}
		}
	}
	
	
if ((!empty($first_name)) && (!empty($last_name)) && (!empty($phonenumber)) && (is_numeric($phonenumber)) && (!empty($email)) && (!empty($streetaddress)) && (!empty($city)) && (!empty($state)) && (!empty($zip)) && (is_numeric($zip)) && (!empty($colors)) && (!empty($description)) && ($response['success'] == true)) {
//End of form validation
	
	
//This section establishes a connection to the mysqli database, and if it fails display error message
	
  $query = "INSERT INTO quotes (first_name, last_name, phonenumber, companyname, " .
    "email, streetaddress, city, state, zip, yes_or_no, vneck, crewneck, hoodie, other_style, " .
	"shortsleeve, longsleeve, other_length, yes_or_no2, colors, heavyweight, lightweight, " .
	"men_xs, men_s, men_m, men_l, men_xl, men_2xl, men_3xl, men_4xl, men_5xl, men_6xl, " . 
    "women_xs, women_s, women_m, women_l, women_xl, women_2xl, women_3xl, women_4xl, women_5xl, women_6xl, " .
	"youth_xs, youth_s, youth_m, youth_l, youth_xl, youth_2xl, youth_3xl, youth_4xl, youth_5xl, youth_6xl, " .
	"toddler_xs, toddler_s, toddler_m, toddler_l, toddler_xl, toddler_2xl, toddler_3xl, toddler_4xl, toddler_5xl, toddler_6xl, " .
	"glitter, glitterflake, foil, vinyl, flock, rhinestones, neon, glow, reflective, hologram, pebblepuff, glaze, patterns, twill, airflow, " .
	"facebook, internet_search, referral, description) " .
    "VALUES ('$first_name', '$last_name', '$phonenumber', '$companyname', " .
	"'$email', '$streetaddress', '$city', '$state', '$zip', '$yesorno', '$vneck', '$crewneck', '$hoodie', '$other_style', " .
	"'$shortsleeve', '$longsleeve', '$other_length', '$yes_or_no2', '$colors', '$heavyweight', '$lightweight', " .
	"'$men_xs', '$men_s', '$men_m', '$men_l', '$men_xl', '$men_2xl', '$men_3xl', '$men_4xl', '$men_5xl', '$men_6xl', " .
	"'$women_xs', '$women_s', '$women_m', '$women_l', '$women_xl', '$women_2xl', '$women_3xl', '$women_4xl', '$women_5xl', '$women_6xl', " .
	"'$youth_xs', '$youth_s', '$youth_m', '$youth_l', '$youth_xl', '$youth_2xl', '$youth_3xl', '$youth_4xl', '$youth_5xl', '$youth_6xl', " .
	"'$toddler_xs', '$toddler_s', '$toddler_m', '$toddler_l', '$toddler_xl', '$toddler_2xl', '$toddler_3xl', '$toddler_4xl', '$toddler_5xl', '$toddler_6xl', " .
	"'$glitter', '$glitterflake', '$foil', '$vinyl', '$flock', '$rhinestones', '$neon', '$glow', '$reflective', '$hologram', '$pebblepuff', '$glaze', '$patterns', '$twill', '$airflow', " .
    "'$facebook', '$internet_search',  '$referral', '$description')";
		
  $result = mysqli_query($dbc, $query)
    or die('Error querying database.');

  mysqli_close($dbc);
  
  $to = 'quotes@top5pct.com';
  $subject = 'Quote Requested';
  $msg = "$first_name $last_name\n" .
    "Telephone Number: $phonenumber\n" .
    "Company name, if any: $companyname\n" .
	"Email: $email\n" .
	"Street Address: $streetaddress\n" .
    "City/State/Zip: $city $state $zip\n" .
	"Do You Have Artwork? $yesorno\n" .
	"Style of Shirt: $vneck $crewneck $hoodie\n" .
	"Other Style of Shirt: $other_style\n" .
	"Pick Shirt Length: $shortsleeve $longsleeve\n" .
	"Other Length Shirts: $other_length\n" .
	"Do you have your own goods? $yes_or_no2\n" .
	"Colors: $colors\n" .
	"Fabric: $heavyweight $lightweight\n" .
	"Men Sizes: XS $men_xs Small $men_s Medium $men_m Large $men_l Xlarge $men_xl 2XL $men_2xl 3XL $men_3xl 4XL $men_4xl 5XL $men_5xl 6XL $men_6xl\n" .
	"Women Sizes: XS $women_xs Small $women_s Medium $women_m Large $women_l Xlarge $women_xl 2XL $women_2xl 3XL $women_3xl 4XL $women_4xl 5XL $women_5xl 6XL $women_6xl\n" .
	"Youth Sizes: XS $youth_xs Small $youth_s Medium $youth_m Large $youth_l Xlarge $youth_xl 2XL $youth_2xl 3XL $youth_3xl 4XL $youth_4xl 5XL $youth_5xl 6XL $youth_6xl\n" .
	"Toddler Sizes: XS $toddler_xs Small $toddler_s Medium $toddler_m Large $toddler_l Xlarge $toddler_xl 2XL $toddler_2xl 3XL $toddler_3xl 4XL $toddler_4xl 5XL $toddler_5xl 6XL $toddler_6xl\n" .
	"Specialty Material: Glitter $glitter Glitter Flake $glitterflake Foil $foil Vinyl $vinyl Flock $flock Rhinestones $rhinestones Neon $neon Glow $glow Reflective $reflective Hologram $hologram Pebble Puff $pebblepuff Glaze $glaze Patterns $patterns Twill $twill Airflow $airflow\n" .
	"How did you hear about us? $facebook $internet_search $referral\n" .
    "$description\n";
 $mail =  mail($to, $subject, $msg, 'From:' . $email);
  
  if($mail){
	  header("Location: https://www.top5pct.com/database/request_quote_form/thank-you.php?fn=".$first_name);
	  exit();
  }
//Display the user input in an confirmation page	
	
  echo '<h2><center>Thank you for your submission!</center></h2><br/>';	
  echo '<font size="4px"><center>' . $first_name . ', a Top 5% representative will contact you soon </center></font><br />';
  echo '<br><br><br><p><center><strong>FILE FORMATS ACCEPTED</strong></center></p><br/>';
  echo '<p><center>.jpg (resolution 300dpi at printed size)<br>';
  echo '.svg<br>';
  echo '.ai<br>';
  echo '.psd<br>';
  echo '.eps<br>';
  echo '.pdf<br></center><br/>';
  echo '</p><strong><center><a href="https://script.google.com/macros/s/AKfycbyV9n_YrelO9qJz1O9fr24AY5WiFsVWDJsVz-P0Gn5efQTWUwyF/exec" title="Upload Art to Top 5%" target="_blank">CLICK HERE TO UPLOAD YOUR ARTWORK!!!</a></center></strong><br />';
  
  	}//end of validated data and adding recored to databse. Closes the code to send the form.
	
} //end of isset condition. This closes the isset and tells us if the form was submitted.
 
else { //if the form has never been submitted, then show it anyway
	$output_form = true;
}
	
if ( $output_form ) { //we will only show the form if the user has error OR not submitted.
	
?>
<div id="tl-box">
	<h1 class="req-quote">Request Your Quote</h1>

	<p>
		<center>
			<font size="5px">Please fill out all form fields:</font>
		</center>
	</p>
	<p>
		<center>
			<font size="3px">"Any artwork you may have can be uploaded after you submit this form"</font>
		</center>
	</p><br />
	<form id="quote_form" class="tl-form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?> ">
		<div class="upper-wrap">
			<div class="input-container">
				<input type="text" name="firstname" size="35" maxlength="33" id="firstname"
					value="<?php echo $first_name; ?>" />
				<label for="firstname">First name:</label>
			</div>
			<div class="inputbox-r input-container">
				<input type="text" name="lastname" id="lastname" size="35" maxlength="33" value="<?php echo $last_name; ?>" />
				<label for="lastname">Last name:</label>
			</div>
			<div class="input-container">
				<input type="text" name="phonenumber" size="35" maxlength="33" id="phonenumber"
					value="<?php echo $phonenumber; ?>" />
				<label for="phonenumber">Telephone:</label>
			</div>
			<div class="input-container">
				<input type="text" name="companyname" size="35" maxlength="33" id="companyname"
					value="<?php echo $companyname; ?>" />
				<label for="companyname">Company Name:</label>
			</div>
			<div class="input-container">
				<input type="text" name="email" size="35" maxlength="33" id="email" value="<?php echo $email; ?>" />
				<label for="email">Email Address:</label>
			</div>
			<div class="input-container">
				<input type="text" name="zip" size="35" maxlength="33" id="zip" value="<?php echo $zip; ?>" />
				<label for="zip">Zip Code:</label>
			</div>
			<div class="input-container">
				<input type="text" name="city" size="35" maxlength="33" id="city" value="<?php echo $city; ?>" />
				<label for="city">City:</label>
			</div>
			<div class="input-container">
				<input type="text" name="state" size="35" maxlength="33" id="state" value="<?php echo $state; ?>" />
				<label for="state">State:</label>
			</div>
			<div class="input-container">
				<input type="text" name="streetaddress" size="35" maxlength="33" id="streetaddress"
					value="<?php echo $streetaddress; ?>" />
				<label for="streetaddress">Street Address:</label>
			</div>
		</div>
		<div class="inner-wrap">
			<div id="legal" class="tl-pdng-btm">
				<label for="yesorno">Have you verified, and are you legally authorized to use or reproduce any artwork you
					submit? By selecting yes you agree to accept full legal responsibility from any copyright owners of the art
					you submit to Top 5%, LLC, and agree to hold Top 5%, LLC harmless from any lawsuits regarding your
					submission.</label>
			</div>
			<!-- Radio button -->
			<div class="tl-radio-wrap">
				<div class="radio">
					<input type="radio" name="yes_or_no" value="Yes" id='r1' <?php if ($yesorno=="yes") echo "checked"; ?>>
					<label for="r1">Yes</label>
				</div>
				<div class="radio">
					<input type="radio" name="yes_or_no" value="N/A" id="r2" <?php if ($yesorno=="n/a") echo "checked"; ?>>
					<label for="r2">I do not have artwork</label>
				</div>
			</div>
		</div>

		<!-- End of Radio Buttons -->

		<!-- Input checkboxes -->
		<div class="tl-boxwrap shirt-style">
			<div class="inner-wrap">
				<label class="tl-align-l">Pick Style of Shirts:</label>
				<div class="tl-radio-wrap">
					<div class="toggle">
						<input type="checkbox" name="vneck" value="vneck" id='c1' <?php if ($vneck)echo "checked";?>>
						<label for="c1">
							V-Neck
						</label>
					</div>
					<div class="toggle">
						<input type="checkbox" name="crewneck" value="crewneck" id="c2" <?php if ($crewneck)echo "checked";?>>
						<label for="c2">
							Crew Neck
						</label>
					</div>
					<div class="toggle">
						<input type="checkbox" name="hoodie" value="hoodie" id="c3" <?php if ($hoodie)echo "checked";?>>
						<label for="c3">
							Hoodie
						</label>
					</div>
				</div>
			</div>
			<div class="inner-wrap">
				<label>Pick Shirt Length:</label>
				<div class="tl-radio-wrap">
					<div class="toggle">
						<input type="checkbox" name="shortsleeve" value="shortsleeve" id="c4"
							<?php if ($shortsleeve)echo "checked";?>>
						<label for="c4">
							Short Sleeve
						</label>
					</div>
					<div class="toggle">
						<input type="checkbox" name="longsleeve" value="longsleeve" id="c5" <?php if ($longsleeve)echo "checked";?>>
						<label for="c5">
							Long Sleeve
						</label>
					</div>
				</div>
			</div>
			<div class="inner-wrap">
				<label class="tl-align-l" for="other_style">Other Style Shirts:</label>
				<textarea name="other_style" id="other_style" rows="5" cols="50"><?php echo $other_style; ?></textarea>
			</div>

			<div class="inner-wrap">
				<label class="tl-align-l" for="other_length">Other Length Shirts:</label>
				<textarea name="other_length" id="other_length" rows="5" cols="50"><?php echo $other_length; ?></textarea>
			</div>
		</div>

		<!-- Radio button -->
		<div class="inner-wrap">
			<label>Do you have your own goods?</label>
			<div class="tl-radio-wrap">
				<div class="radio">
					<input type="radio" name="yes_or_no2" value="Yes" id="goods" <?php if ($yes_or_no2=="yes") echo "checked"; ?>>
					<label for="goods">Yes</label>
				</div>
				<div class="radio">
					<input type="radio" name="yes_or_no2" value="No" id="goods1" <?php if ($yes_or_no2=="no") echo "checked"; ?>>
					<label for="goods1">No</label>
				</div>
			</div>
		</div>
		<!-- End of Radio Buttons -->
		<div id="legal" class="tl-pdng-btm">
			<label for="yesorno">If yes, Top 5%, will need to know the brand, fabric and style before acceptance. We cannot
				guarantee supplied goods against defects or imperfections. Items must be clean and 1-2 extra for
				spoilage.</label>
		</div>
		<div class="tl-boxwrap ">
			<div class="">
				<div class="inner-wrap">
					<label class="tl-block" for="colors">Colors:</label>
					<textarea name="colors" id="colors" rows="5" cols="50"><?php echo $colors; ?></textarea>
				</div>
			</div>
			<!-- Input checkboxes -->
			<div class="inputbox-r">
				<div class="inner-wrap">
					<label>Fabric:</label>
					<div class="tl-radio-wrap">
						<div class="toggle">
							<input type="checkbox" name="heavyweight" value="heavyweight" id="fabric1"
								<?php if ($heavyweight)echo "checked";?>>
							<label for="fabric1">
								Heavyweight
							</label>
						</div>
						<div class="toggle">
							<input type="checkbox" name="lightweight" value="lightweight" id="fabric2"
								<?php if ($lightweight)echo "checked";?>>
							<label for="fabric2">
								Lightweight
							</label>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End of Input Checkboxes -->

		<div>
			<table class="tl-table" border="5">
				<tbody>
					<tr class="top_row">
						<th scope="col">&nbsp;</th>
						<th scope="col">MEN</th>
						<th scope="col">WOMEN</th>
						<th scope="col">YOUTH</th>
						<th scope="col">TODDLER</th>
					</tr>
					<tr>
						<th scope="col">XS</th>
						<td>
							<center><input type="text" name="men_xs" size="5" maxlength="5" id="men_xs"
									value="<?php echo $men_xs; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="women_xs" size="5" maxlength="5" id="women_xs"
									value="<?php echo $women_xs; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_xs" size="5" maxlength="5" id="youth_xs"
									value="<?php echo $youth_xs; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_xs" size="5" maxlength="5" id="toddler_xs"
									value="<?php echo $toddler_xs; ?>" /></center>
						</td>
					</tr>
					<tr>
						<th scope="col">S</th>
						<td>
							<center><input type="text" name="men_s" size="5" maxlength="5" id="men_s" value="<?php echo $men_s; ?>" />
							</center>
						</td>
						<td>
							<center><input type="text" name="women_s" size="5" maxlength="5" id="women_s"
									value="<?php echo $women_s; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_s" size="5" maxlength="5" id="youth_s"
									value="<?php echo $youth_s; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_s" size="5" maxlength="5" id="toddler_s"
									value="<?php echo $toddler_s; ?>" /></center>
						</td>
					</tr>
					<tr>
						<th scope="col">M</th>
						<td>
							<center><input type="text" name="men_m" size="5" maxlength="5" id="men_m" value="<?php echo $men_m; ?>" />
							</center>
						</td>
						<td>
							<center><input type="text" name="women_m" size="5" maxlength="5" id="women_m"
									value="<?php echo $women_m; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_m" size="5" maxlength="5" id="youth_m"
									value="<?php echo $youth_m; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_m" size="5" maxlength="5" id="toddler_m"
									value="<?php echo $toddler_m; ?>" /></center>
						</td>
					</tr>
					<tr>
						<th scope="col">L</th>
						<td>
							<center><input type="text" name="men_l" size="5" maxlength="5" id="men_l" value="<?php echo $men_l; ?>" />
							</center>
						</td>
						<td>
							<center><input type="text" name="women_l" size="5" maxlength="5" id="women_l"
									value="<?php echo $women_l; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_l" size="5" maxlength="5" id="youth_l"
									value="<?php echo $youth_l; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_l" size="5" maxlength="5" id="toddler_l"
									value="<?php echo $toddler_l; ?>" /></center>
						</td>
					</tr>
					<tr>
						<th scope="col">XL</th>
						<td>
							<center><input type="text" name="men_xl" size="5" maxlength="5" id="men_xl"
									value="<?php echo $men_xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="women_xl" size="5" maxlength="5" id="women_xl"
									value="<?php echo $women_xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_xl" size="5" maxlength="5" id="youth_xl"
									value="<?php echo $youth_xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_xl" size="5" maxlength="5" id="toddler_xl"
									value="<?php echo $toddler_xl; ?>" /></center>
						</td>
					</tr>
					<tr>
						<th scope="col">2XL</th>
						<td>
							<center><input type="text" name="men_2xl" size="5" maxlength="5" id="men_2xl"
									value="<?php echo $men_2xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="women_2xl" size="5" maxlength="5" id="women_2xl"
									value="<?php echo $women_2xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_2xl" size="5" maxlength="5" id="youth_2xl"
									value="<?php echo $youth_2xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_2xl" size="5" maxlength="5" id="toddler_2xl"
									value="<?php echo $toddler_2xl; ?>" /></center>
						</td>
					</tr>
					<tr>
						<th scope="col">3XL</th>
						<td>
							<center><input type="text" name="men_3xl" size="5" maxlength="5" id="men_3xl"
									value="<?php echo $men_3xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="women_3xl" size="5" maxlength="5" id="women_3xl"
									value="<?php echo $women_3xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_3xl" size="5" maxlength="5" id="youth_3xl"
									value="<?php echo $youth_3xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_3xl" size="5" maxlength="5" id="toddler_3xl"
									value="<?php echo $toddler_3xl; ?>" /></center>
						</td>
					</tr>
					<tr>
						<th scope="col">4XL</th>
						<td>
							<center><input type="text" name="men_4xl" size="5" maxlength="5" id="men_4xl"
									value="<?php echo $men_4xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="women_4xl" size="5" maxlength="5" id="women_4xl"
									value="<?php echo $women_4xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_4xl" size="5" maxlength="5" id="youth_4xl"
									value="<?php echo $youth_4xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_4xl" size="5" maxlength="5" id="toddler_4xl"
									value="<?php echo $toddler_4xl; ?>" /></center>
						</td>
					</tr>
					<tr>
						<th scope="col">5XL</th>
						<td>
							<center><input type="text" name="men_5xl" size="5" maxlength="5" id="men_5xl"
									value="<?php echo $men_5xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="women_5xl" size="5" maxlength="5" id="women_5xl"
									value="<?php echo $women_5xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_5xl" size="5" maxlength="5" id="youth_5xl"
									value="<?php echo $youth_5xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_5xl" size="5" maxlength="5" id="toddler_5xl"
									value="<?php echo $toddler_5xl; ?>" /></center>
						</td>
					</tr>
					<tr>
						<th scope="col">6XL</th>
						<td>
							<center><input type="text" name="men_6xl" size="5" maxlength="5" id="men_6xl"
									value="<?php echo $men_6xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="women_6xl" size="5" maxlength="5" id="women_6xl"
									value="<?php echo $women_6xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="youth_6xl" size="5" maxlength="5" id="youth_6xl"
									value="<?php echo $youth_6xl; ?>" /></center>
						</td>
						<td>
							<center><input type="text" name="toddler_6xl" size="5" maxlength="5" id="toddler_6xl"
									value="<?php echo $toddler_6xl; ?>" /></center>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<div>
			<h3>Standard Finishes: HTV - Digital - Laser - Screenprint</h3>
		</div>

		<!-- Input checkboxes -->
		<div class="specialty-finishes">
			<h3>Specialty Finishes:</h3>

			<div class="toggle">
				<input type="checkbox" name="glitter" value="1" id="glitter" <?php if ($glitter)echo "checked";?>>
				<label for="glitter">
					Glitter <a href="../../php/specialty_materials/glitter_shirts_joliet_plainfield_romeoville.php"
						target="_blank" alt="Custom Glitter Shirts made in Joliet, Illinois"></a>
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="foil" value="1" id='foil' <?php if ($foil)echo "checked";?>>
				<label for="foil">
					Foil <a href="../../php/specialty_materials/custom_foil_shirts_joliet_romeoville.php"
						alt="Get a custom made Foil shirt by Top 5 Percent, LLC."></a>
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="vinyl" value="1" id="vinyl" <?php if ($vinyl)echo "checked";?>>
				<label for="vinyl">
					Vinyl <a href="../../php/specialty_materials/custom_vinyl_shirts_joliet_plainfield.php" target="_blank"
						alt="Vinyl apparel can be customized here in Joliet, Plainfield and Romeoville."></a>
				</label>
			</div>


			<div class="toggle">
				<input type="checkbox" name="flock" value="1" id="flock" <?php if ($flock)echo "checked";?>>
				<label for="flock">
					Flock <a href="../../php/specialty_materials/flock_shirts_joliet_romeoville_plainfield.php" target="_blank"
						alt="Customized flock apparel can be 	purchased here in Joliet, Crest Hill and Shorewood."></a>
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="rhinestones" value="1" id="Rhinestones" <?php if ($rhinestones)echo "checked";?>>
				<label for="Rhinestones">
					Rhinestones <a href="../../php/specialty_materials/rhinestone_shirts_rhinestones_for_clothing_joliet.php"
						target="_blank"
						alt="Custom rhinestone shirts and other apparel made in Joliet, Bolingbrook and Rockdale."></a>
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="neon" value="1" id="neon" <?php if ($neon)echo "checked";?>>
				<label for="neon">
					Neon
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="glow" value="1" id="glow" <?php if ($glow)echo "checked";?>>
				<label for="glow">
					Glow <a href="../../php/specialty_materials/glow_shirts_joliet_new_lenox.php" target="_blank"
						alt="Glow shirts are custom made by Top 5 Percent in Joliet, Plainfield and Romeoville."></a>
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="reflective" value="1" id="Reflective" <?php if ($reflective)echo "checked";?>>
				<label for="Reflective">
					Reflective <a href="../../php/specialty_materials/reflective_shirts_jackets_hats_joliet.php" target="_blank"
						alt="Corporate wear with reflective material made by Top 5 Percent, LLC in Joliet, Lockport and Shorewood."></a>
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="hologram" value="1" id="Holographic" <?php if ($hologram)echo "checked";?>>
				<label for="Holographic">
					Holographic <a href="../../php/specialty_materials/holographic_shirts_holograph_apparel_joliet.php"
						target="_blank" alt="Beautiful holographic apparel can be bought in Joliet, Romeoville and Plainfield."></a>
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="pebblepuff" value="1" id="pebble" <?php if ($pebblepuff)echo "checked";?>>
				<label for="pebble">
					Pebble Puff
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="glaze" value="1" id="glaze" <?php if ($glaze)echo "checked";?>>
				<label for="glaze">
					Glaze
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="patterns" value="1" id="patterns" <?php if ($patterns)echo "checked";?>>
				<label for="patterns">
					Patterns <a href="../../php/specialty_materials/custom_shirt_patterns_joliet_bolingbrook.php" target="_blank"
						alt="Many different types of custom pattern clothes can be made in Joliet, Rockdale, Crest Hill and Lockport."></a>
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="twill" value="1" id="twill" <?php if ($twill)echo "checked";?>>
				<label for="twill">
					Twill
				</label>
			</div>
			<div class="toggle">
				<input type="checkbox" name="airflow" value="1" id="air-flow" <?php if ($airflow)echo "checked";?>>
				<label for="air-flow">
					Air Flow
				</label>
			</div>

		</div>
		<!-- End of Input Checkboxes -->

		<!-- Input checkboxes -->
		<div class="inner-wrap">
			<label>How did you hear about us?</label>
			<div class="tl-radio-wrap">
				<div class="toggle">
					<input type="checkbox" name="facebook" value="facebook" id="fb" <?php if ($facebook)echo "checked";?>>
					<label for="fb">
						Facebook
					</label>
				</div>
				<div class="toggle">
					<input type="checkbox" name="internet_search" value="internet_search" id="internet-search"
						<?php if ($internet_search)echo "checked";?>>
					<label for="internet-search">
						Internet Search
					</label>
				</div>
				<div class="toggle">
					<input type="checkbox" name="referral" value="referral" id="referral" <?php if ($referral)echo "checked";?>>
					<label for="referral">
						Referral
					</label>
				</div>
			</div>
		</div>
		<!-- End of Input Checkboxes -->
		<div class="inner-wrap">
			<label for="description">Extra Notes:</label>
			<textarea name="description" id="description" rows="5" cols="50"><?php echo $description; ?></textarea>
		</div>
		<div class="tl-recapctha">
			<div class="g-recaptcha" data-sitekey="6LdCXB0UAAAAAJD3DSsL8tNSLFjrdR1d-caBIgSf"></div>
		</div>
		<div id="submit">
			<button type="submit" class="send">Submit</button>
		</div>
	</form>
</div>
<?php  
  }
  ?>

<?php include 'footer.php' ?>