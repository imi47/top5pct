<?php include 'header.php' ?>

<a class="faqs-banner">
	<span>Have</span> <span>Questions?</span>
	<!-- <br> -->
	<span>We</span> <span>Have</span> <span>Answers.</span>
</a>

<div class="customer-review">
	<script type="text/javascript">
		var review_token = 'p8zTh9U63lY2CqrsAeYXsRiw4jv5Zjf3dyLYsOhuw31M8yBWDT';
		var review_target = 'review-container'; 
	</script>
	<script src="https://reviewsonmywebsite.com/js/embed.js?v=8" type="text/javascript"></script>
	<div id="review-container"></div>
</div>

<ol class="faqs-content">
	<li>
		<h1>No Minimum Order</h1>
		<p>You can order 1 shirt or more</p>
	</li>
	<li>
		<h1>Turnaround Time</h1>
		<p>(most orders are completed sooner rather later)
			1 - 10 pieces up to 5 Working Days <br>
			11 - 19 Pieces up to 7 Working Days <br>
			20 - 50 Pieces up to 14 Working Days <br>
			More than 50 is on a case-by-case basis. <br>
			<strong>Same Day Orders</strong> can be made, but it’s on a first come first served
			time frame. <br>
			<strong>Rush Orders</strong> - Customers that require a garment be produced
			in under 3 days will be assessed a rush charge.
			In many cases we produce garments in under 3 days without
			having to add a rush charge and without they need for a client to
			request it. However, when there are times when that is not
			possible. If you feel it’s necessary to get your job done ahead of
			everyone else then a rush charge will be added to your order.
			Getting a job done in front of someone else does not mean that
			the previous customers will not get their jobs out on time. We’ve
			never been late on an order and we aim to continue with that
			level of service. All rush order items will be assess an an extra
		</p>
	</li>
	<li>
		<h1>GARMENT CHOICE</h1>
		<p>We keep on hand a select number and brand
			of garments. If you select a garment that we do not stock in store,
			but find it in our online catalog we typically can get the garments in
			1 day if it’s in stock at our suppliers location. We stock many different
			brands and styles in house, so in most cases our customers choose one
			of our stocked items.</p>
	</li>
	<li>
		<h1>COLORS AND STYLE</h1>
		<p>We can provide you with tons of different
			colors, styles and fabric types to fit your needs. Crewnecks, V-Necks,
			Raglans, Long Sleeve, Tank Tops, Hoodies, Joggers, Caps, Bags and
			much more. Our prices are based on extra small to extra large shirts
			only. 2XL and up are additional. We can go as large as 6XL custom shirts.</p>
	</li>
	<li>
		<h1>CAN YOU BRING YOUR OWN GARMENTS</h1>
		<p>tems brought into
			our shop. If supplying your own garments we recommend providing
			one garment extra for testing purposes. We need to acheive the right
			settings in order to produce a quality product and the only way to
			acheive that on unfamiliar garments is to first run a test before we
			place the order into full production.</p>
	</li>
	<li>
		<h1>ARTWORK AND DESIGN TIME</h1>
		<p>
			Many customers find many images
			off the internet, but what they don’t understand is that images found
			on the internet have been made to be viewed on the internet
			only and not meant for printing. Most of these file are low resolution
			files which helps speed up load time for webpages. However, taking
			these images and enlarging them to be printed on a garment is a
			big no no. We can print anything, but do you want a quality print
			or bad print? Our guess is that you would like a quality print. <br>
			Below are a list of file format recommendations and in order of
			preference. <br>
			.ai
			.cdr<br>
			.eps <br>
			.svg <br>
			.pdf <br>
			.psd <br>
			.jpg and .png - We recommend 300dpi resolution, but a minimum
			150dpi. If you don’t understand resolution or dpi, which
			we’ve found out that most customer who supply their
			own files don’t, then attempt to send a file that is at least
			100,000kb in file size or more for a decent image print.
			We prefer no less than 500,000kb or higher file size, but
			bare minimum 100,000kb. If you’re still not sure if your
			file is print ready you’re welcome to upload your file
			here. Remember what you give is what you get. <br>
			Send us an email notifying us that you have uploaded
			a piece of artwork that you would like us to review. <br>
			If you do not have any artwork and need a design made from scratch.
			We offer 10 minutes of design time free of charge. If we feel the design
			will take longer than 10 minutes we will inform you of this before we
			begin. If we complete the design at 10 minutes and the customer
			decides to make changes after we provide the proof. We charge a
			minimum of $25.00 on top of the original price for ANY additional
			design time. Our hourly rate for design time is $50hr. $25 for 30
			minutes. Call to get an estimate of design time costs. We will not
			provide any design services or any other work without a non-
			refundable 50% deposit. If Top 5%, LLC designs an image and the
			design time was waived and the customer received the image free
			of design charges because we were able to design it in a timely fashion.
			Top 5%, LLC owns the right to that design and the customer cannot
			reuse, resale, reprint or reproduce the image by any other company
			without prior approval from an authorized Top 5%, LLC representative.
			If you would like purchase the design Top 5%, LLC created you may
			contact us to discuss that option. <br>

			<strong>Save Money and DIY</strong> you can save on design time cost by designing
			your own t-shirt by using our <strong>FREE</strong> Online T-Shirt Designer that
			includes free clipart and text. Design your own custom t-shirt here:
			https://top5pct.com/opencart_store/index.php
		</p>
	</li>
	<li>
		<h1>What Type of Printing Options Do We Offer?</h1>
		<ul>
			<li><strong>Digital Printing</strong> - Our digital printing process allow are customers
				to print with as many color as they want with at no additional
				cost onto a printable heat transfer vinyl. Unlike screen printing
				where the cost goes up based on the number of colors. Our
				digital prints give you crisp clean prints on and can be applied to
				most garment types with no ink migration (100% cotton, 50/50
				Blends, 100% Polyester, Tri-Blends, Nylon and more).</li>
			<li>
				<strong>Screen Printing</strong> - We require a minimum order of 20 shirts and
				recommend 100% cotton shirts for screen printing to elimate dye
				migration. The more colors the more it cost for screen printing.
				We can produce large quantity orders efficiently with screen
				printing.
			</li>
			<li><strong>Dye-Sublimation Printing</strong> - Our dye sub printer gives you a
				virtually 100% soft feel. You can barely feel the image on the shirt
				or at all. This printing process requires 100% polyester shirt only.
				These shirts are great for performance wear. If you do not want
				that performance shirt feel, we also offer 100% polyester shirts
				with a cotton feel. So if you’re looking for that true soft hand
				image on a shirt that feel pretty close to 100% cotton then
				sublimation is a great choice. However, the drawback to
				producing this specific type of custom shirt is that you can’t
				apply it to dark colors. It can only be used for light colored shirts
				(white, eggshell, light grays, pastel colors, etc.).</li>

			<li>
				<strong>Laser Printer</strong> - Our laser printer has the abilty to print white ink.
				So, if you’re looking for truly detailed and colorful images that
				contain true white ink this machine can get the job done. The
				one drawback to our machine is the size limitation. It can only
				print up to an 8 x 10 image, but the results are amazing. With
				laser printing we can apply the print to 100% Cotton, 100%
				polyester, 50/50 Blend, leather and almost any fabric available.
			</li>
		</ul>
	</li>
	<li>
		<h1>PROOFS</h1>
		<p>We supply proofs to all customer’s prior to printing. We
			will not produce any jobs without a customer’s written approval. When
			reviewing proofs keep in mind that there will be some variations from
			digital to real life. When reviewing a proof on your computer monitor
			remember the color may look slightly different on the next monitor.
			Most computer monitors are calibrated differently which means the
			proof may look a little different from one monitor to the next. Also,
			when viewing digital proof keep in mind that these are digital
			representations of the shirt and layout you will be getting, so the
			postitioning may be slightly different when printed in real life. With
			that said, we do our best to match the color and position as close as
			possible to the proof we send to you.</p>
	</li>
	<li>
		<h1>RETURNS</h1>
		<p>All sales are final on custom designed apparel. We never
			print any jobs without the customer’s approval. We email digital proofs
			to every customer prior to printing and move ahead only when the
			customer reviews the proof and grants permission to print. Once a
			customer approves a proof and give us the go ahead to produce the
			order there are no refunds.</p>
	</li>
	<li>
		<h1>CARE INSTRUCTIONS</h1>
		<p>Proper care can extend the life of your
			custom garment. As with most of our custom garments we recommend
			you turn the garments inside out prior to washing. Do not use bleach
			or any other additive similar to bleach, such as whiteners and
			brighteners. Wash in cold water and set dryer on a warm setting. Do
			not iron the printed image.</p>
	</li>
	<li>
		<h1>COPYRIGHT IMAGES</h1>
		<p>Many customer will find copyright images
			off the internet, send them to us and ask us to print it. We cannot print
			every image you find off the internet. You have to have written
			permission from the owner to use their image. If you do not have
			written permission to use an image that is copywritten we will not
			print it. We suggest you use our free t-shirt designer to design your
			own image for free, hire Top 5 Percent, LLC to design a custom
			image for you or purchase your image stock art website and send to
			us for printing.</p>
	</li>
	<li>
		<h1>EMBROIDERY</h1>
		<p>We do not specialize in embroidery however, we
			can embroider your garment on a small scale. We can embroider to
			shirts, smocks, jackets, aprons, etc. Basically flat surfaces, but not hats.</p>
	</li>
	<li>
		<h1>SIGNAGE</h1>
		<p>We can produce everything from vehicle magnets,
			wall/window/floor/vehicle decals, stickers promotional items and
			more. We use the best material for the job. Most signage jobs are
			priced by the square foot and labor for installation, if needed. The
			square foot price varies based of the type of material needed to
			produce the job.</p>
	</li>
	<li>
		<h1>DECALS AND VEHICLE WRAPS</h1>
		<p>When asking for installation
			of decals or vehicle wraps always thoroughly wash your vehicle prior
			to bringing it to us or we will have to charge for washing your vehicle.
			Do not wax or polish your vehicle prior getting decals or wraps
			installed. It could affect the adhesion of the decal/wrap to the body or
			window of the vehicle. Oversized vehicle would not fit into our garage
			and would have to have decals applied outside in the right temperature
			Small SUV’s and under can get decals and wraps applied indoors.</p>
	</li>
</ol>

<?php include 'footer.php' ?>