<?php include 'header.php' ?>

<img src="images/firewatch.jpg" alt="" class="full-width-image">

<div class="customer-review">
	<script type="text/javascript">
		var review_token = 'p8zTh9U63lY2CqrsAeYXsRiw4jv5Zjf3dyLYsOhuw31M8yBWDT';
		var review_target = 'review-container';
	</script>
	<script src="https://reviewsonmywebsite.com/js/embed.js?v=8" type="text/javascript"></script>
	<div id="review-container"></div>
</div>

<!-- these images are wrapped in a container so that the first image will always be on the left when the parent div is targetted using the nth-child() selector -->
<div>
	<div class="img-with-text custom-shirt">
		<img data-scroll src="images/firewatch.jpg" alt="">
		<div>
			<h1>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt sunt fugiat quod in illo accusantium rem
				voluptatem culpa minus temporibus!</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
		</div>
	</div>

	<div class="img-with-text custom-shirt">
		<img data-scroll src="images/1.jpg" alt="">
		<div>
			<h1>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt sunt fugiat quod in illo accusantium rem
				voluptatem culpa minus temporibus!</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
		</div>
	</div>

	<div class="img-with-text custom-shirt">
		<img data-scroll src="images/firewatch.jpg" alt="">
		<div>
			<h1>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt sunt fugiat quod in illo accusantium rem
				voluptatem culpa minus temporibus!</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
		</div>
	</div>

	<div class="img-with-text custom-shirt">
		<img data-scroll src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<div>
			<h1>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt sunt fugiat quod in illo accusantium rem
				voluptatem culpa minus temporibus!</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
		</div>
	</div>

	<div class="img-with-text custom-shirt">
		<img data-scroll src="images/firewatch.jpg" alt="">
		<div>
			<h1>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt sunt fugiat quod in illo accusantium rem
				voluptatem culpa minus temporibus!</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
		</div>
	</div>
</div>

<div class="swatch-colors">
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
	<div data-scroll>
		<img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>color name</span>
	</div>
</div>

<div class="pics-with-text">
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
	<div data-scroll><img src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<span>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Molestias, consequatur.</span>
	</div>
</div>

<iframe class="commercial" src="https://www.youtube.com/embed/j0vslNa91gU" frameborder="0"
	allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<?php include 'footer.php' ?>