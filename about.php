<?php include 'header.php' ?>

<img src="images/2.jpg" alt="" class="full-width-image">

<!-- these images are wrapped in a container so that the first image will always be on the left when the parent div is targetted using the nth-child() selector -->
<div>
	<div class="img-with-text">
		<img data-scroll src="images/firewatch.jpg" alt="">
		<div>
			<h1>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt sunt fugiat quod in illo accusantium rem
				voluptatem culpa minus temporibus!</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
		</div>
	</div>

	<div class="img-with-text">
		<img data-scroll src="https://source.unsplash.com/user/erondu/1600x900" alt="">
		<div>
			<h1>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
			<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Deserunt sunt fugiat quod in illo accusantium rem
				voluptatem culpa minus temporibus!</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
		</div>
	</div>

	<div class="img-with-text">
		<img data-scroll src="images/firewatch.jpg" alt="">
		<div>
			<h1>Lorem, ipsum dolor sit amet consectetur adipisicing elit.</h1>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
			<p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Accusamus, odit?</p>
		</div>
	</div>
</div>

<iframe class="commercial" src="https://www.youtube.com/embed/j0vslNa91gU" frameborder="0"
	allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<?php include 'footer.php' ?>