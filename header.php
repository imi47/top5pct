<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Design Custom T-Shirts and Banners in Joliet | Top 5 Percent</title>
	<meta name="keywords"
		content="tees,dog shirts,hoodies,bags,custom underwear,tote bag,team uniforms,dog tees,posters,undies,panties,raglan,ringer,vneck,crewneck,youth shirts,will county" />
	<meta name="viewport"
		content="width=device-width; initial-scale=1.0; maximum-scale=1.0; minimum-scale=1.0; user-scalable=no; target-densityDpi=device-dpi" />

	<meta name="description"
		content="Create personal shirts or corporate apparel, also signs, dog clothes, aprons, smocks, totes, promotional and gift items with our free web tools or we'll build it. Customize baby clothes, bibs, basketball uniforms, football uniforms, spirit wear, vehicle magnets, and flock printing with no minimum orders. Some materials we use are Rhinestone, foil, and vinyl shirts, window and wall graphics, etc. Our company offers laser, digital, dye-sublimation, as well as screen printing. Other specialities we provide are window clings, decals, stickers and invitations. District 202, Shorewood, Plainfield, Crest Hill" />
	<link rel="stylesheet" href="css/style.css">
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="css/lightbox.min.css">
	<link rel="stylesheet" href="css/flickity.min.css">
	<link rel="stylesheet" href="css/lightgallery.min.css">
	<script src="https://kit.fontawesome.com/ffa8a2c96b.js"></script>

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-105728444-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag() { dataLayer.push(arguments); }
		gtag('js', new Date());

		gtag('config', 'UA-105728444-1');
	</script>
	<!-- End of Google Analytics Code -->

	<!-- Google Search Console Code -->
	<meta name="google-site-verification" content="tdogVrtoIxkc2Oe0zvKNTqmeg87YUBe229RCs8MW1kU" />
	<!-- End of Google Search Console Code -->
</head>

<body>
	<header>
		<a href="index.php" class="logo"><img src="images/logo.gif" alt=""></a>
		<div class="icons">
			<a href="#"><i class="fab fa-google"></i></a>
			<a href="#"><i class="fab fa-facebook-f"></i></a>
			<a href="#"><i class="fab fa-instagram"></i></a>
			<a href="#"><i class="fab fa-pinterest"></i></a>
		</div>
		<div>
			<span>Telephone: (815) 349-8600</span>
			<span>Hours M-F 8:30am - 6pm</span>
			<span>Best by Appointment</span>
			<a href="#">Sign Up To Get Special Offers</a>
			<a href="contact.php">Contact Us</a>
		</div>
	</header>
	<nav class="hidden">
		<a href="index.php">Home</a>
		<a href="about.php">About Us</a>
		<div>
			<a href='javascript:undefined'>Get Quote</a>
			<div>
				<a href="custom-shirt.php">Custom Shirts</a>
				<a href="#">All Others</a>
			</div>
		</div>
		<div>
			<a href='javascript:undefined'>Custom Apparel</a>
			<div>
				<a href="custom-shirt.php">Custom Shirts</a>
				<a href="#">Reunion Shirts</a>
				<a href="#">Corporate Wear</a>
				<a href="#">Spirit Water</a>
			</div>
		</div>
		<a href="#">DIY</a>
		<a href="#">Signs</a>
		<div>
			<a href='javascript:undefined'>Decals</a>
			<div>
				<a href="#">Vehicle Decals</a>
				<a href="#">Wall Decals</a>
				<a href="#">Window Decals</a>
			</div>
		</div>
		<a href="#">Stickers</a>
		<a href="#">Magnets</a>
		<a href="#">Promotional Items</a>
		<div>
			<a href='javascript:undefined'>Stores</a>
			<div>
				<a href="#">Top 5% Clothing</a>
				<a href="#">Black Doll Clothing</a>
			</div>
		</div>
		<a href="#">Portfolio</a>
		<i class="fas fa-caret-down"></i>
	</nav>
	<div class="container">